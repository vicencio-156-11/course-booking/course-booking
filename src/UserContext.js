import React from 'react'

// This module was created to provide the necessarry informtaion of the user/client who is using the app


// 1. Create the 'context' about the subject
	// createContext() will allow us to create a context 'object' that will allow us to store informtaion about the subject.
	const UserContext = React.createContext(); //container for the information

// 2. Acquire for the provider property of the newly created context object
	// visualize:
	// UserContext = {
	// 	propsAboutTheUser: value
		// Provider:
	// }

// Provider => this will allow us to use its utility to permit subcomponents of our app to consume/use the available information about our subject. You need to expose the utility/data
export const  UserProvider = UserContext.Provider;

//3. Identify the module on your app who will take mantle/role of the 'provider' component.

//Consumers
// EXPOSE the context object for the consumers
export default UserContext;