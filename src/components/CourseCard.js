// identify needed components
import {Card} from 'react-bootstrap';

// element with routing capability
import {Link} from 'react-router-dom';
// we are doing this to use this link component to not only redirect 

export default function CourseCard({courseProp}){
	return(
			<Card className= ' courseCard m-4 d-md-inline-flex d-sm-inline-flex'>
				<Card.Body >
					<Card.Title>
						{courseProp.name}
					</Card.Title>
				</Card.Body>
				<Card.Body >
					<Card.Text>
						{courseProp.description}
					</Card.Text>
				</Card.Body>
				<Card.Body  >
					<Card.Text>
						Price: {courseProp.price}
					</Card.Text>
				</Card.Body>
					<Link to={`view/${courseProp._id}`}className = " btn btn-success">
						View Course
					</Link>
			</Card>
	);
};