import Hero from './../components/Banner';
import {Form, Container} from 'react-bootstrap';
import Swal from 'sweetalert2';


const data = {
	title: 'Update Course Page',
	content: 'Update Course details below.'
}
	
	
export default function Update() {

	const updateCourse = (eventSubmit) => {
		eventSubmit.preventDefault()
		return(
			Swal.fire({
				icon:'success',
				title: 'The course has been created successfully!',
				text: 'Thank you!'
			})
		);
	};

	return(
		<div>
			<Hero bannerData={data} />
			<Container>
				<h1 className="text-center">Update Form</h1>
				<Form onSubmit={e => updateCourse(e)}>
					<Form.Group>
						<Form.Label>Name:</Form.Label>
						<Form.Control type="text" placeholder="Enter course title here" required />
					</Form.Group>
						<Form.Label>Description:</Form.Label>
						<Form.Control type="text" placeholder="Enter course description here" required/>
                    <Form.Group>
                    	<Form.Label> Price:</Form.Label>
                    	<Form.Control type="number" placeholder="Enter course price here" required />
                    </Form.Group>
						  <Form.Check 
						    type="switch"
						    id="custom-switch"
						    label="Enable Course"
						  />
						  
				</Form>
			</Container>
		</div>
	);
};


