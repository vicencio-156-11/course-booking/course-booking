// [Activity]

// [MAIN GOAL] Create and Add Course Page

// 1. Acquire all needed component to build the page
// 2. Identify all the info needed to create a new course. (name, description, price)
// 3. Expose the data to the entry point and assign a designated endpoint for the page
// 4. Re-render the navbar to add a new nav item component for the Add Course Page
// 5. Create a simulation that will describe the workflow if the user would want to create a new course.
// 6. Submit the project repo in boodle



// identify the components needed to create the register page.

import Hero from '../components/Banner';
import {Container, Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';



const data = {
	title: 'Create a New Course',
	content: 'Fill out the fields to create a new course.'
}

export default function addCourse() {

	// catch the 'click' event that will happen on the butoon component
	const newCourse = (eventSubmit) => {
		eventSubmit.preventDefault()
		return(
			// display a message that will confirm to the user that registration is successfull.
			Swal.fire({
				icon:'success',
				title: 'A new course has been successfully created',
				text: 'Thank you!'
			})
		);
	};


	return(
		<div>
			<Hero bannerData={data}/>
			
			<Container>
				<h1 className="text-center">Add a New Course</h1>
				<Form onSubmit={e => newCourse(e)}>
					<Form.Group>
						<Form.Label>Course Title:</Form.Label>
						<Form.Control type="text" placeholder="Enter Course Title" required />
					</Form.Group>
						<Form.Label>Description:</Form.Label>
						<Form.Control type="text" placeholder="Enter the Course Description" required/>
                    <Form.Group>
                    	<Form.Label> Price:</Form.Label>
                    	<Form.Control type="number" placeholder="Enter the Price of the Course per unit" required />
                    </Form.Group>

                    	{/*Submit Button*/}
                    <Button variant="success" className= "btn-block" type="submit"> Submit </Button>
				</Form>
			</Container>
		</div>
	);
};


