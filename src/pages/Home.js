// acquire all the component that will make up the home page (hero section, highlights)

import Banner from './../components/Banner'
import Highlights from './../components/Highlights'

//Let's create a data object that will describe the content of the hero section
const data = {
	title: 'Welcome to ARMV University',
	content: 'Knowledge within our grasp'
}




export default function Home() {
	return(
		<div>
			<Banner bannerData={data}/>
			<Highlights/>
		</div>
	);
};
